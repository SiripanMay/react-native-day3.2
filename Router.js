import React, {Component} from 'react'
import { Route, NativeRouter, Switch,Redirect } from 'react-router-native'
import LoginPage from './LoginPage'
import ProfilePage from './ProfilePage'
import ListPage from './ListPage'
import ProductPage from './ProductPage'
import EditProfile from './EditProfile'
import EditProduct from './EditProduct'
import AddProduct from './AddProduct'



class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path='/LoginPage' component={LoginPage}/>
                    <Route exact path='/ProfilePage' component={ProfilePage}/>
                    <Route exact path='/ListPage' component={ListPage}/>
                    <Route exact path='/ProductPage' component={ProductPage}/>
                    <Route exact path='/EditProfile' component={EditProfile}/>
                    <Route exact path='/EditProduct' component={EditProduct}/>
                    <Route exact path='/AddProduct' component={AddProduct}/>
                    <Redirect to="/LoginPage" />
                </Switch>
            </NativeRouter>
        )
    }
}
export default Router