import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon,Flex } from '@ant-design/react-native';
class ProductPage extends Component {
    
    goToLoginPage = () => {
        this.props.history.push('/LoginPage')
    }

    goToListPage = () => {
        this.props.history.push('/ListPage')
    }
    goToEditProduct = () => {
        this.props.history.push('/EditProduct')
    }
    render() {
        return (
            <View style={[styles.container]}>
            <View style={[styles.header, styles.center]}>
                    <TouchableOpacity onPress={this.goToListPage}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.box, styles.center]}>
                        <Text style={styles.textHeader}>Product</Text>
                    </View>
                </View>

                <View style={[styles.content]}>
                   
                </View>
                <View style={[styles.footer]}>
                <Flex.Item style={{ paddingLeft: 4, paddingRight: 4, margin: 10 }}>
                    <Button onPress={this.goToEditProduct}>Edit Product</Button>
                </Flex.Item>
            </View>

            </View>


        )
    }
}
export default ProductPage

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
    },
    footer: {
        flexDirection: 'row',
    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,
        
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#FADBD8',
        flex: 0,
       
    },
});

