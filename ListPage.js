import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert,ScrollView } from 'react-native';
import { Button, Icon,Flex,Card } from '@ant-design/react-native';
class ListPage extends Component {
    
    goToLoginPage = () => {
        this.props.history.push('/LoginPage')
    }
    goToProfilePage = () => {
        this.props.history.push('/ProfilePage',{username:this.props.location.state.username})
    }
    goToAddProduct = () => {
        this.props.history.push('/AddProduct')
    }
    goToProductPage = () => {
        this.props.history.push('/ProductPage')
    }
    render() {
        return (
            <View style={[styles.container]}>
            <View style={[styles.header]}>
            <TouchableOpacity onPress={this.goToLoginPage}>
            <View style={[styles.boxIcon, styles.center]}>
                    <Text style={styles.textHeader}> <Icon name="left" size="md" color="black" /> </Text>
            </View>
            </TouchableOpacity>
            <View style={[styles.box, styles.center]}>
                    <Text style={styles.textHeader}> ListPage </Text>
                    </View>
            </View>
<ScrollView >
                <View style={[styles.content]}>
                
                <View style={styles.row}>
                <TouchableOpacity onPress={this.goToProductPage}>
            <View style={[styles.box1,styles.center]}>
                <Text style={styles.textHead}>Lorem...1</Text>
            </View>
            </TouchableOpacity>
            <View style={[styles.box2,styles.center]}>
                <Text style={styles.textHead}>Lorem...2</Text>
            </View>
          </View>
                </View>
                <View style={[styles.content]}>
                
                <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
                <Text style={styles.textHead}>Lorem...1</Text>
            </View>
            <View style={[styles.box2,styles.center]}>
                <Text style={styles.textHead}>Lorem...2</Text>
            </View>
          </View>
                </View>
                <View style={[styles.content]}>
                
                <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
                <Text style={styles.textHead}>Lorem...1</Text>
            </View>
            <View style={[styles.box2,styles.center]}>
                <Text style={styles.textHead}>Lorem...2</Text>
            </View>
          </View>
                </View>
                <View style={[styles.content]}>
                
                <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
                <Text style={styles.textHead}>Lorem...1</Text>
            </View>
            <View style={[styles.box2,styles.center]}>
                <Text style={styles.textHead}>Lorem...2</Text>
            </View>
          </View>
                </View>
                <View style={[styles.content]}>
                
                <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
                <Text style={styles.textHead}>Lorem...1</Text>
            </View>
            <View style={[styles.box2,styles.center]}>
                <Text style={styles.textHead}>Lorem...2</Text>
            </View>
          </View>
                </View>
</ScrollView>
                <View style={[styles.footer]}>
                <Flex.Item style={{ paddingLeft: 4, paddingRight: 4 ,margin:10}}>
                    <Button onPress={this.goToLoginPage}>Login</Button>
                </Flex.Item>
                <Flex.Item style={{ paddingLeft: 4, paddingRight: 4 ,margin:10}}>
                    <Button onPress={this.goToProfilePage}>Profile</Button>
                </Flex.Item>
                <Flex.Item style={{ paddingLeft: 4, paddingRight: 4 ,margin:10}}>
                    <Button onPress={this.goToAddProduct}>Add</Button>
                </Flex.Item>
                </View>

            </View>


        )
    }
}
export default ListPage

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        margin:5
    },
    row: {
        flex: 1,
        flexDirection: 'row'
      },
      box1: {
        backgroundColor: 'green',
        flex: 1,
        margin: 14,
      },
      box2: {
        backgroundColor: 'purple',
        flex: 1,
        margin: 14,
      },
        
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        backgroundColor: '#FADBD8',
    },
    box: {
        backgroundColor: '#FADBD8',
        flex: 1,
        
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        backgroundColor: '#FADBD8',
        flex: 0,
       
    },
});

